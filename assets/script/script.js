'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2020-05-26T17:01:17.194Z',
    '2020-07-28T23:36:17.929Z',
    '2023-11-08T10:51:36.790Z',
  ],
  currency: 'PHP',
  locale: 'en-PH'
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};




const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

/////////////////////////////////////////////////

/* ====================== Functions ====================== */
const formatMovementDate = (date, locale) => {
  
  const calcDaysPassed = (date1, date2) => Math.round(Math.abs(date2 - date1) / (1000 * 60 * 60 * 24));

  const dayPassed = calcDaysPassed(new Date(), date);
  console.log(dayPassed);

  if(dayPassed === 0){
    return `Today`;
  } else if(dayPassed === 1){
      return `Yesterday`;
  } else if (dayPassed <= 7){
      return `${dayPassed} days ago`;
  } else {
      return new Intl.DateTimeFormat(locale).format(date);
  } 
}

// Format Currency
const formatCurr = (value, locale, currency) => {
  // Formatted Currency
    return new Intl.NumberFormat(locale, {
      style: 'currency',
      currency: currency
    }).format(value);
}


// Display Movements || Activity
const displayMovements = (acc, sort = false) => {

  containerMovements.innerHTML = '';

  const movs= sort ? acc.movements.slice().sort((a,b) => a - b) : acc.movements;

  movs.forEach( (mov, i) => {
    const type = mov > 0 ? 'deposit' : 'withdrawal';

    const date = new Date(acc.movementsDates[i]);

    const displayDate = formatMovementDate(date, acc.locale);
    
    const formattedMov = formatCurr(mov, acc.locale, acc.currency);

    const html = `
          <div class="movements__row">
            <div class="movements__type movements__type--${type}">${i + 1} ${type}</div>
            <div class="movements__date">${displayDate}</div>
            <div class="movements__value">${formattedMov}</div>
          </div>
    `;
    /* .insertAdjacentHTML() method
          - does not reparse the element it is being used on, and thus not corrupt the existing elements inside that element.
          - This avoid extra step of serialization, making it much faster than direct "innerHTML" manipulation
          - takes 2 strings (position, text(containing an HTML to insert))

          SYNTAX:
                element.insertAdjacentHTML(position, text);
    */
    containerMovements.insertAdjacentHTML('afterbegin', html)
  });

}

// Creating Balance Function
const calcDisplayBalance = acc => {
  acc.balance = acc.movements.reduce((acc, mov) => acc + mov, 0);

  labelBalance.textContent = formatCurr(acc.balance, acc.locale, acc.currency);
}


// Display Account Summary
const calcDisplaySummary = acc => {
  const incomes = acc.movements
    .filter(mov => mov > 0)
    .reduce((acc, mov) => acc + mov, 0);
  
  const outcomes = acc.movements         
    .filter(mov => mov < 0)
    .reduce((acc, mov) => acc + mov, 0);

  const interest = acc.movements
    .filter(mov => mov > 0)
    .map(deposit => deposit * acc.interestRate/100)
    .filter(interest => interest >= 1)
    .reduce((acc, interest) => acc + interest, 0);


  labelSumIn.textContent = formatCurr(incomes, acc.locale, acc.currency);
  labelSumOut.textContent = formatCurr(Math.abs(outcomes), acc.locale, acc.currency);
  labelSumInterest.textContent = formatCurr(interest, acc.locale, acc.currency);

}

// Update UI
const updateUI = acc => {
  // Display Movements
  displayMovements(acc);

  // Display balance
  calcDisplayBalance(acc);

  // Display Summary
  calcDisplaySummary(acc);
}

// Creating accounts username Function
const createUsernames = accts => {
  accts.forEach(acc => {
    acc.username = acc.owner
      .toLowerCase()
      .split(' ')
      .map(name => name[0])
      .join('');
  });
}
createUsernames(accounts);


const startLogOutTimer = () => {
  const tick = () => {
    const min = String(Math.trunc(time / 60)).padStart(2, 0);
    const sec = String(time % 60).padStart(2, 0);
    // In each call, print the remaining time to UI
    labelTimer.textContent = `${min}:${sec}`;

    // When 0 seconds, stop the timer and logout user
    if(time === 0){
      clearInterval(timer);
      labelWelcome.textContent = `Log in to get Started`;
      containerApp.style.opacity = 0;
    }
    // Decrease 1s
    time--;
  }


  // Set time to 5 mins
  let time = 300;

  // Call the timer every seconds
  tick();
  const timer = setInterval(tick, 1000);

  return timer;
}

/* ====================== Event Handlers ====================== */

let currentAccount, timer;

// FAKE ALWAYS LOGGEDIN
// currentAccount = account1;
// updateUI(currentAccount);
// containerApp.style.opacity = 100;



// ===== Login Event =====
btnLogin.addEventListener('click', e => {
  // Prevent form from submitting
  e.preventDefault();

  currentAccount = accounts.find(acc => acc.username === inputLoginUsername.value);

  if(currentAccount?.pin === +inputLoginPin.value){
    // Display UI
    labelWelcome.textContent = `Welcome back, ${currentAccount.owner.split(' ')[0]}`;
    containerApp.style.opacity = 100;

    // Create Current Date & time
    const now = new Date();
    // Options for value [numeric, long, short, narrow]
    const options = {
      hour: 'numeric',
      minute: 'numeric',
      day: 'numeric',
      month: 'short',
      year: 'numeric',
      weekday: 'short'
    }

    // Getting the locale language from the browser
    // const locale = navigator.language;

    labelDate.textContent = new Intl.DateTimeFormat(currentAccount.locale, options).format(now);

    // Clear Input Fields
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();

    // Display Timer
    if(timer){
      clearInterval(timer);
    }
    timer = startLogOutTimer();

    // Update UI
    updateUI(currentAccount);
  }
});


// ===== Transfer Event =====
btnTransfer.addEventListener('click', e => {
  e.preventDefault();

  const amount = +inputTransferAmount.value;

  const receiverAcc = accounts.find(acc => acc.username === inputTransferTo.value);

  inputTransferAmount.value = inputTransferTo.value = '';

  if(amount > 0 && receiverAcc && currentAccount.balance >= amount && receiverAcc?.username !== currentAccount.username) {
    // Doing the transfer
    currentAccount.movements.push(-amount);
    receiverAcc.movements.push(amount);

    // Add Transfer Date
    currentAccount.movementsDates.push(new Date().toISOString());
    // Add Receiver Date
    receiverAcc.movementsDates.push(new Date().toISOString());

    // Update UI
    updateUI(currentAccount);

    // Reset Timer
    clearInterval(timer);
    timer = startLogOutTimer();
  }
});

// ===== Loan Event =====
btnLoan.addEventListener('click', e => {
  e.preventDefault();

  const amount = Math.floor(inputLoanAmount.value);

  if(amount > 0 && currentAccount.movements.some(mov => mov >= amount * 0.1) ) {

    setTimeout(() => {
      // Add movement
      currentAccount.movements.push(amount);

      // Add Loan Date
      currentAccount.movementsDates.push(new Date().toISOString());

      // Update UI
      updateUI(currentAccount);

      // Reset Timer
    clearInterval(timer);
    timer = startLogOutTimer();

    }, 2500);
    
    inputLoanAmount.value = "";
  }
});


// ===== Close Account Event =====
btnClose.addEventListener("click", e => {
  e.preventDefault ();

  if ((currentAccount.username === inputCloseUsername.value) && (currentAccount.pin === +inputClosePin.value)) {
    const index = accounts.findIndex(acc => acc.username === currentAccount.username);

    // // Delete account
    accounts.splice(index, 1);

    // // Hide UI
    containerApp.style.opacity = 0;
  }

  inputCloseUsername.value = inputClosePin.value = "";
});



// ===== Sorting Balance Event =====
// Create a variable "STATE" to monitor the state of "btnSort"
let sorted = false;

btnSort.addEventListener("click", e => {
  e.preventDefault();

  displayMovements(currentAccount.movements, !sorted);

  // Flip the sorted state
  sorted = !sorted; // everytime the btnSort is clicked it is flipped to True || false
});


// ===== Label Balance Event =====
labelBalance.addEventListener('click', () => {
  const movementsUI = Array.from(document.querySelectorAll('.movements__value'), el => +el.textContent.replace('₱', ''));

  console.log(movementsUI);
});